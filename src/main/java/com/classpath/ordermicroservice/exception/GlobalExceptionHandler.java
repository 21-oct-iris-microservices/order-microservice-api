package com.classpath.ordermicroservice.exception;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.*;

import static org.springframework.http.HttpStatus.BAD_REQUEST;

@Component
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {

    @ExceptionHandler(IllegalArgumentException.class)
    public ResponseEntity<Error> handleInvalidReqData(IllegalArgumentException exception){
        log.error("Passed invalid request data , {}", exception.getMessage());
        return ResponseEntity.status(BAD_REQUEST).body(new Error(100, exception.getMessage()));
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Map<Integer, List<String>>> handleInvalidInputData(MethodArgumentNotValidException exception){
        log.error("Passed invalid request data , {} ", exception.getMessage());
        List<String> errorList = new ArrayList<>();
        var errors = exception.getAllErrors();
        errors.forEach(error -> errorList.add(error.getDefaultMessage()));
        Map<Integer, List<String>> errorMap = new HashMap<>();
        errorMap.put(400, errorList);

        //clean code
      /*  var entries = errorMap.entrySet();
        var iterator = entries.iterator();
        while(iterator.hasNext()){
            var entry = iterator.next();
            entry.getKey();
            entry.getValue();
        }*/

        return ResponseEntity.status(BAD_REQUEST).body(errorMap);
    }
}


@RequiredArgsConstructor
@Getter
class Error {
    private final int code;
    private final String message;
}