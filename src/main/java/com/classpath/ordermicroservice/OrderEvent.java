package com.classpath.ordermicroservice;

import com.classpath.ordermicroservice.model.EventType;
import com.classpath.ordermicroservice.model.Order;
import lombok.*;

@ToString
@NoArgsConstructor
@Data
@AllArgsConstructor
public class OrderEvent {
    private EventType eventType;
    private Order order;
}