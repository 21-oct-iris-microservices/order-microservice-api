package com.classpath.ordermicroservice.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;
import static javax.persistence.GenerationType.AUTO;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Entity
@Table(name = "orders")

public class Order {

    @Id
    @GeneratedValue(strategy = AUTO)
    private long orderId;

    @Min(value = 20000, message = "Min value should be 20000")
    @Max(value = 50000, message = "Max value cannot be more than 50000")
    private double price;

    @NotEmpty(message = "Customer name cannot be empty")
    private String customerName;

    @Email(message = "Customer email address cannot be empty")
    private String emailAddress;

    @PastOrPresent(message = "Order date cannot be in the future")
    private LocalDate date;

}