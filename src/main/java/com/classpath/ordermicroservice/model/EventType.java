package com.classpath.ordermicroservice.model;

public enum EventType {
    ORDER_CREATED,
    ORDER_PENDING,
    ORDER_PROCESSED,
    ORDER_REJECETD
}
