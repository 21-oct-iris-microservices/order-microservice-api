package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.OrderEvent;
import com.classpath.ordermicroservice.model.EventType;
import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import io.github.resilience4j.retry.annotation.Retry;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import java.time.LocalDate;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;

    private final RestTemplate restTemplate;

    private final KafkaTemplate<Long, OrderEvent> kafkaTemplate;

    @CircuitBreaker(name = "inventorymicroservice" , fallbackMethod = "fetchOrder")
    @Retry(name = "retrycalls", fallbackMethod = "fetchOrder")
    public Order saveOrder(Order order){
        log.info("Making an API call to inventory microservice {}", order);
        log.info("Persisting the Order payload in the broker :: {}", order);
        //final ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://localhost:9222/api/v1/inventory", null, Integer.class);
       // log.info(" Response from inventory service , {} ", responseEntity.getBody());
      //  ProducerRecord<Long, OrderEvent> record = new ProducerRecord<>("orders-topic", order.getOrderId(), new OrderEvent(ORDER_CREATED, order));
        //this.kafkaTemplate.send(record);
        return this.orderRepository.save(order);
    }

    private Order fetchOrder(Exception exception){
        log.error(" Exception while updating the inventory ::  {} ", exception.getMessage());
        return Order.builder().orderId(1111).customerName("ramesh").emailAddress("ramesh@gmail.com").date(LocalDate.now()).build();
    }

    public Map<String, Object> fetchOrders(PageRequest pageRequest){
        final Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        final long totalRecords = pageResponse.getTotalElements();
        final int totalPages = pageResponse.getTotalPages();
        final List<Order> data = pageResponse.getContent();

        final LinkedHashMap<String, Object> linkedHashMap = new LinkedHashMap<>();
        linkedHashMap.put("pages", totalPages);
        linkedHashMap.put("total-records", totalRecords);
        linkedHashMap.put("data", data);
        return linkedHashMap;
    }

    public Order findById(long orderId){
        return this.orderRepository
                .findById(orderId)
                .orElseThrow(() -> new IllegalArgumentException("Invalid order id"));
    }

    public void deleteOrderById(long orderId){
        this.orderRepository.deleteById(orderId);
    }

    public void deleteAllOrders() {
        this.orderRepository.deleteAll();
    }
}