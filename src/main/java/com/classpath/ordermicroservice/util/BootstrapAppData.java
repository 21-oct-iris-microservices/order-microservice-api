package com.classpath.ordermicroservice.util;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import com.github.javafaker.Faker;
import com.github.javafaker.Name;
import com.github.javafaker.Number;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {

    private final OrderRepository orderRepository;

    private final Faker faker = new Faker();

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        IntStream.range(1,100).forEach(index -> {
            final Name fakeName = faker.name();
            Order order = Order.builder()
                                    .customerName(fakeName.firstName())
                                    .price(faker.number().randomDouble(2, 25000, 49000))
                                    .emailAddress(faker.internet().emailAddress().toLowerCase())
                                    .date(faker.date().past(10, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                                    .build();
            this.orderRepository.save(order);
        });
    }
}