package com.classpath.ordermicroservice.controller;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.service.OrderService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping("/api/v1/orders")
@RequiredArgsConstructor
public class OrderRestController {

    private final OrderService orderService;

    @PostMapping
    @ApiResponses(
            {@ApiResponse(
                    responseCode = "201", description = "Succesfully created an order"
            ),
            @ApiResponse(
                    responseCode = "401", description = "Incase of un authenticated user"
            )}
    )
    public Order saveOrder(@RequestBody @Valid @Parameter(name = "order", required = true) Order order){
            return this.orderService.saveOrder(order);
    }

    @GetMapping
    public Map<String,Object> fetchAllOrders(
            @RequestParam(required = false, defaultValue = "10") int items,
            @RequestParam (required = false, defaultValue = "0") int page,
            @RequestParam (defaultValue = "customerName") String sortby) {
        PageRequest pageRequest = PageRequest.of(page, items, Sort.by(sortby));

        return this.orderService.fetchOrders(pageRequest);
    }

    @GetMapping("/{id}")
    public Order fetchOrderById(@PathVariable long id){
        return this.orderService.findById(id);
    }

    @DeleteMapping("/{id}")
    public void deleteOrderById(@PathVariable long id){
        this.orderService.deleteOrderById(id);
    }

    @DeleteMapping
    public void deleteAllRecords(){
        this.orderService.deleteAllOrders();
    }
}