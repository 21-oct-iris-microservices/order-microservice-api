package com.classpath.ordermicroservice.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;

import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;

@Configuration
public class ApplicationSecurityConfiguration extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/v3/**", "/swagger-ui/**", "/swagger-ui/index.html/**", "/swagger-ui.html","/actuator/**", "/h2-console/**", "/login/**", "/logout/**")
                        .permitAll()
                    .antMatchers(GET, "/api/v1/orders/**")
                        .hasAnyAuthority("ROLE_users", "ROLE_admins", "Role_super_admins")
                    .antMatchers(POST, "/api/v1/orders/**")
                        .hasAnyAuthority("ROLE_admins", "ROLE_super_admins")
                    .antMatchers(HttpMethod.DELETE)
                        .hasAuthority("ROLE_super_admins")
                    .anyRequest()
                    .permitAll()
                .and()
                .oauth2ResourceServer()
                .jwt();
    }

    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(){
        final JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
        final JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        grantedAuthoritiesConverter.setAuthoritiesClaimName("groups");
        jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return jwtAuthenticationConverter;
    }
}