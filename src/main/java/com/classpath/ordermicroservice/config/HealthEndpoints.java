package com.classpath.ordermicroservice.config;

import com.classpath.ordermicroservice.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
@Slf4j
class DBHealthIndicator implements HealthIndicator {

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        log.info("Exposing the DB health status :: ");
        final long count = this.orderRepository.count();
        if (count <= 0 ){
            log.warn("DB service seems to be down :: ");
            return Health.down().withDetail("DB Health", "DB service is down ").build();
        }
        return Health.up().withDetail("DB Health", "DB service is up ").build();
    }
}

@Component
@Slf4j
class KafkaHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        log.info("Exposing the Kafka health status :: ");
        return Health.up().withDetail("Kafka Health", "Kafka service is up ").build();
    }
}