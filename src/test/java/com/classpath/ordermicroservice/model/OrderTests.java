package com.classpath.ordermicroservice.model;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTests {

    private static Faker faker;

    private  Order order;

    @BeforeAll
    public static void setUp(){
        faker = new Faker();
    }

    @BeforeEach
    public void init(){
        order = Order.builder().orderId(12).date(LocalDate.now()).price(22000).customerName("Ramesh").emailAddress("ramesh@gmail.com").build();
    }

    @Test
    public void testConstructor(){
        //set the expectations
        //execute
        //assert the behaviour
        assertNotNull(order);
        assertEquals(22000, order.getPrice());
        assertEquals("Ramesh", order.getCustomerName());
        assertEquals("ramesh@gmail.com", order.getEmailAddress());
        assertEquals(LocalDate.now(), order.getDate());
        //verify the expectations
    }

    @Test
    public void testSetCustomerName(){
        order.setCustomerName("Ravindra");
        assertEquals("Ravindra", order.getCustomerName());
    }

    @Test
    public void testNotEquals(){
        Order order1 = Order.builder().orderId(13).date(LocalDate.now()).price(22000).customerName("Ramesh").emailAddress("ramesh@gmail.com").build();
        Order order2 = Order.builder().orderId(12).date(LocalDate.now()).price(22000).customerName("Ramesh").emailAddress("ramesh@gmail.com").build();
        assertFalse(order1.equals(order2));
    }

    @Test
    public void testEquals(){
        Order order1 = Order.builder().orderId(12).date(LocalDate.now()).price(22000).customerName("Ramesh").emailAddress("ramesh@gmail.com").build();
        Order order2 = Order.builder().orderId(12).date(LocalDate.now()).price(22000).customerName("Ramesh").emailAddress("ramesh@gmail.com").build();
        assertTrue(order1.equals(order2));
    }

    @Test
    public void testToString(){
        assertEquals("Order(orderId=12, price=22000.0, customerName=Ramesh, emailAddress=ramesh@gmail.com, date=2021-11-08)", order.toString());
    }
}