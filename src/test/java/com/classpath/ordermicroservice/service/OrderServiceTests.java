package com.classpath.ordermicroservice.service;

import com.classpath.ordermicroservice.model.Order;
import com.classpath.ordermicroservice.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.LocalDate;
import java.util.Optional;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class OrderServiceTests {

    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void findOrderByOrderId(){
        //setup
        //set the expectations
        Order order = Order.builder().orderId(12).date(LocalDate.now()).price(22000).customerName("Ramesh").emailAddress("ramesh@gmail.com").build();
        when(orderRepository.findById(12L)).thenReturn(Optional.of(order));

        //execute
        try {
            final Order fetchedOrder = orderService.findById(12);
            //assert the behavior
            assertNotNull(fetchedOrder);
            assertEquals(12, fetchedOrder.getOrderId());
        } catch (Exception exception){
            fail("This method should not throw exception");
        }

        //verifications
        verify(orderRepository , times(1)).findById(12L);
    }

    @Test
    public void findInvalidOrderByOrderId(){
        //setup
        //set the expectations
        Order order = Order.builder().orderId(12).date(LocalDate.now()).price(22000).customerName("Ramesh").emailAddress("ramesh@gmail.com").build();
        when(orderRepository.findById(12L)).thenReturn(Optional.empty());
        //execute
        try {
            final Order fetchedOrder = orderService.findById(12);
            fail("This method should throw invalid order id");
        } catch (IllegalArgumentException exception){
            assertNotNull(exception);
        }
        //verifications
        verify(orderRepository , times(1)).findById(12L);
    }
}